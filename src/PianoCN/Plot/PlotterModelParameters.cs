﻿//
//  Copyright © 2014 Parrish Husband (parrish.husband@gmail.com)
//  The MIT License (MIT) - See LICENSE.txt for further details.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PiaNO
{
    /// <summary>
    /// 绘图仪模型参数
    /// </summary>
    public class PlotterModelParameters : PiaFile
    {
        public string? ModelPathName;
        public string? ModelBaseName;
        public string? DriverPathName;
        public string? DriverVersion;
        public string? DriverTagline;
        public short TookitVersion;
        public short DriverType;
        public string? CanonicalFamilyName;
        public bool ShowCustomFirst;
        public bool TrueTypeAsText;
        public string? CanonicalModelName;
        public string? LocalizedFamilyName;
        public string? LocalizedModelName;
        public bool FileOnly;
        public long ModelAbilities;
        public string? UDMDescription;
        public string? DeviceName;
        public string? DriverName;
        public string? Shortname;
        public string? FriendlyName;
        public short DmDriverVersion;
        public bool DefaultSystemConfig;
        public string? Platform;
        public string? Locale; // Check for hex

        // Mod
        // > Media
        //  > Size
        // > Description
        // Del
        // > Media
        // Udm
        // > Calibration
        // > Media
        // Hidden
        // > Media
    }
}
