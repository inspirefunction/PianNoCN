﻿//
//  Copyright © 2014 Parrish Husband (parrish.husband@gmail.com)
//  The MIT License (MIT) - See LICENSE.txt for further details.
//

namespace PiaNO
{
    /// <summary>
    /// 纸张数据
    /// </summary>
    public class Media
    {
        public long Abilities; // 位
        public int CapsState;  // 位?
        public long UIOwner;
        public double SizeMaxX ; // 一些编码
        public double SizeMaxY ; // ^
    }
}
